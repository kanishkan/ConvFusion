#!/usr/bin/env python
from setuptools import setup, find_packages

from codecs import open
from os.path import abspath, dirname, join
import sys

__version__ = "1.0.0"

this_dir = abspath(dirname(__file__))
with open(join(this_dir, 'README.md'), encoding='utf-8') as file:
    long_description = file.read()

setup(
    name='convfuser',
    version=__version__,
    description='Explore layer fusion scheduling for CNNs',
    long_description=long_description,
    url='http://gitlab.com/SergantSeagal/fusion',
    author='Luc Waeijen',
    author_email='lucwaeijen@gmail.com',
    license='Beerware',
    packages=find_packages(),
    install_requires=[
        'networkx',
        'pillow',
        'keras',
        'numpy',
        'pydot'
    ],
    entry_points={
        'console_scripts': [
            'convfuser=convfuser.cli:main',
        ],
    }
)
