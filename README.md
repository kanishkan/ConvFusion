ConvFuser
=========
Explore schedules for CNNs to optimize external memory traffic versus on-chip memory size and number of multiply-accumulates.

Finding the optimimal schedule for a CNN is not trivial, as the many inherently independent operations in CNNs result in a vast scheduling space.
This tool is an embodiment of the models described in [ConvFusion: A Model for Layer Fusion in Neural Networks](https://doi.org/10.1109/ACCESS.2021.3134930), and allows model-based exploration of the scheduling space.
Supported code transformations include loop tiling, reordering, explicit scheduling of data movements between memory levels, and loop/layer fusion.

An example of a pareto optimal scheduling of a small 3 layer network is illustrated below.
The input to the network is the image on the left, which gets fed into consecutive layers L0, L1 and L2.
These layers represent the intermediate results and final output.
However the working set of data, which is kept in local buffers, is visualized at the bottom half of the image.
Note that the results of L0 and L1 do not need to be stored, only the working set in the buffers is live at any given point in time.
This is the power of layer fusion, keeping intermediate results in small local buffers until their consumption.
![Animated fusion schedule](https://gitlab.com/lwaeijen/ConvFusion/-/raw/master/fused_cat.gif?inline=false)


Getting Started
===============
The tool consists of a python package, and as such can be downloaded and installed using pip.
However, to ensure all tool dependencies are met, the recommended way is to use the docker image packed with this tool.
You may either roll your own using the supplied Docker file, or download the pre-built container from the container registry.
An easy shorthand to launch the docker container is added to the Makefile.
Make sure to configure the `DOCKER_USER` variable in the Makefile before using these commands.
Once set up, all that is required to start a container is issuing:<br />
`make docker-start`

Usage
=====
The simplest method to use the tool is place an h5 file of your model in the model directory.
Alternatively you may also supply a python file that generates an h5 using Keras.
Check the model directory for examples.

With your `model[.h/.py]` in place, simple issue:<br />
`make <model>`

This will automatically install the python package in the container, and start an exploration of the scheduling space.
The final resulting Pareto schedules are stored in the output folder in `<model>.ds`.
This design space file is in fact a JSON file, so you can easily parse it or manually inspect it.
A plot of the Pareto front will also be placed in the output directory.

There are many more features and options to tune the exploration available through the commandline.
See below for the various arguments and their meaning:
```
usage: convfuser [-h]
                 (--network {ResNet50,ResNet101,ResNet152,MobileNet,MobileNetV2,VGG16,VGG19,InceptionV3,Xception} | --h5-model H5_MODEL)
                 [--no-canonicalize] [--model-cache MODEL_CACHE_DIR]
                 --input-image INPUT_IMAGE [--space SPACE_FNAME]
                 [--max-fused MAX_FUSED] [--min-fused MIN_FUSED]
                 [--exploration-cap EXP_CAP]
                 [--tiling {none,powers-of-two,exact,extremes}]
                 [--no-loopreorder] [--no-output-contrib] [--buffered]
                 [--recompute] [--no-pareto] [--dot DOT_FNAME]
                 [--dot-seg SEG_DOT_FNAME] [--save-space SAVE_SPACE_FNAME]
                 [--save-plot PLOT_FNAME] [--hide-results] [--idx IDX]
                 [--trace] [--verify] [--debug] [--quiet]

optional arguments:
  -h, --help            show this help message and exit

Input Settings:
  --network {ResNet50,ResNet101,ResNet152,MobileNet,MobileNetV2,VGG16,VGG19,InceptionV3,Xception}
                        Use builtin network on imagenet
  --h5-model H5_MODEL   Keras h5 model file
  --no-canonicalize     Disable canonicalization of network graph
  --model-cache MODEL_CACHE_DIR
                        Optional directory for caching builtin models
  --input-image INPUT_IMAGE
                        Input image
  --space SPACE_FNAME   Optional database file with previously explored
                        scheduling space

Schedule Space Search Settings:
  --max-fused MAX_FUSED
                        Maximum number of fused layers (>=1)
  --min-fused MIN_FUSED
                        Minimum number of fused layers (>=1)
  --exploration-cap EXP_CAP
                        Abort segment exploration after examining N schedules
  --tiling {none,powers-of-two,exact,extremes}
                        Which tilings to consider (Default powers-of-two)
  --no-loopreorder      Fix loop order to: [m,n,i,o]
  --no-output-contrib   Ignore contribution of output (accesses & memory
                        footprint)
  --buffered            Consider buffering complete first layer of each fused,
                        recomputed segment
  --recompute           Consider recomputation of fused layers
  --no-pareto           Return all points, not only pareto dominant.

Output Settings:
  --dot DOT_FNAME       Write out dotfile of network graph
  --dot-seg SEG_DOT_FNAME
                        Write out dotfile of graph with fused layers for
                        selected point
  --save-space SAVE_SPACE_FNAME
                        Store database file with explored scheduling space
  --save-plot PLOT_FNAME
                        Store pareto plot of design space
  --hide-results        Hide post-processed network output

Execution Settings:
  --idx IDX             Index of point to run. When omitted or negative visual
                        selection will be presented
  --trace               Halide trace accesses and memory sizes
  --verify              Verifies ALL found pareto schedules
  --debug               Print debug output on verification failure
  --quiet               Print less
```

License
========
This work is licensed under creative commons attribution-ShareAlike 4.0
In case you use this work, please cite the [accompanying paper](http://doi.org/10.1109/ACCESS.2021.3134930).
When using bibtex you can use the following entry:
```
@ARTICLE{9646923,
  author={Waeijen, Luc and Sioutas, Savvas and Peemen, Maurice and Lindwer, Menno and Corporaal, Henk},
  journal={IEEE Access},
  title={ConvFusion A Model for Layer Fusion in Convolutional Neural Networks},
  year={2021},
  volume={},
  number={},
  pages={1-1},
  doi={10.1109/ACCESS.2021.3134930}}
```

Repository icon made by [Smashicons](https://smashicons.com/) from [www.flaticon.com](https://www.flaticon.com/)
