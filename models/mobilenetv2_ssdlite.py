#!/usr/bin/env python3
from mobilenet_v2_ssdlite_keras.models.graphs.mobilenet_v2_ssdlite_praph import mobilenet_v2_ssdlite
import keras
from keras import layers


inp = layers.Input(shape=(300, 300, 3))
out = mobilenet_v2_ssdlite(inp)
model = keras.Model(
    inputs=[inp],
    outputs=[out],
    name=__file__[:-3]
)
model.summary()
model.save(__file__.replace('.py', '.h5'))
