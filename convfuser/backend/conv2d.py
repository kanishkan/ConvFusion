import halide as hl
import numpy as np
from ..utils import listify
from .halidefunc import HalideFunc


class Conv2D(HalideFunc):
    """
    2D convolution function
    """

    def __init__(self, layer, input):

        # init halide func
        super().__init__(layer)

        # Load variables into shorthands
        b = self.b
        i = self.i
        o = self.o
        m = self.m
        n = self.n
        D = self.D
        S = self.S
        name = self.name

        # Load variables into shorthands
        use_bias = self.use_bias

        # get input and output tensors
        inputs = listify(layer.input)
        assert (
            len(inputs) == 1 and "Only support Conv2D with single input"
        )

        outputs = listify(layer.output)
        assert (
            len(outputs) == 1 and "Only support Conv2D with single output"
        )

        assert (
            layer.data_format in ['channels_last', 'NHWC'] and
            "Only NHWC layout supported"
        )

        # Get weights and biases from layer
        if use_bias:
            self.weights, self.bias = self.get_weights()
        else:
            self.weights = self.get_weights()[0]

        # Weights buffer
        self.weights_host = hl.Buffer(
            np.reshape(self.weights, (D.l, D.k, D.i, D.o)),
            name + '_weights_host'
        )
        self.weights_device = hl.Func(name + '_weights_device')
        self.weights_device[n, m, i, o] = self.weights_host[n, m, i, o]

        # Bias buffer
        self.bias_device = None
        if use_bias:
            self.bias_host = hl.Buffer(
                np.reshape(self.bias, (D.o)), name + '_bias_host'
            )
            self.bias_device = hl.Func(name + '_bias_device')
            self.bias_device[o] = self.bias_host[o]

        # Input buffer
        if isinstance(input, HalideFunc):
            self.input_unpadded = input.f
        elif isinstance(input, np.ndarray):
            self.input_unpadded = hl.Buffer(input, name + '_input_host')
        else:
            print(
                "Error: unknown input type %s passed to conv2d" %
                (type(input))
            )
            exit(-1)

        # Add padding to input
        if self.padding_type == 'same':
            # N.B.: buffer passed to condition can NOT be the same as the variable
            # that's written to (i.e. b_input != b_input_unpadded)
            self.input_data = hl.BoundaryConditions.constant_exterior(
                self.input_unpadded,
                0.,
                bounds=[(0, D.b), (0, self.in_height),
                        (0, self.in_width), (0, D.i)]
            )
        else:
            # valid padding
            self.input_data = self.input_unpadded

        # input_local_buffer wrapper
        # WORKAROUND TO USE HALIDES OWN 'in' FUNCTION:
        # fin = getattr(self.f, 'in')
        self.ldata = hl.Func(name + "_input_data_local")
        self.ldata[b, n, m, o] = self.input_data[b, n, m, o]

        # Main Function
        conv = hl.Func(name+'_conv')

        # Reduction Domain
        r = hl.RDom(
            [
                (0, D.k),  # x
                (0, D.l),  # y
                (0, D.i),  # z
            ],
            name + "_kernel"
        )
        self.r = r

        # Load bias, or init with zeroes
        conv[b, n, m, o] = self.bias_device[o] if self.use_bias else 0.

        # Compute convolution
        conv[b, n, m, o] += \
            self.weights_device[r.y, r.x, r.z, o] * \
            self.ldata[b, n * S.n + r.y - self.pad_n,
                       m * S.m + r.x - self.pad_m, r.z]

        # Apply activation function if required
        if self.act.type == 'relu':
            conv[b, n, m, o] = hl.min(
                self.act.max_value, hl.max(0., conv[b, n, m, o])
            )

        # Set main function
        self.f = conv

        # Set output shape
        self.shape = [D.b, D.n, D.m, D.o]

    def schedule(
        self, S, last, fuse_tgt=None, fuse_sched=None, verbose=False
    ):
        # shorthands
        f = self.f
        weights_device = self.weights_device

        # weights go completely root
        weights_device.store_root()
        weights_device.compute_root()

        # untiled order
        uorder = [ll[0] for ll in S.order[0:4] if ll[0] != 'i']

        # reorder local buffer
        self.ldata.reorder(*map(lambda x: getattr(self, x), uorder))

        # We never want to recompute the convolution, store root
        self.f.store_root()
        # self.f.store_at(last.f, hl.Var.outermost())

        # tile if this is the last layer
        if last.f is self.f:
            # Apply Tiling but only of relevant dimension
            if S.order[4][0] == 'n':
                order = [
                    l[0] if l[0] != 'n' else l for l in S.order
                    if l[-1] == 'i' or l[0] == 'n']
                f.update().split(
                    self.n, self.n_o, self.n_i, S.T.n,
                    hl.TailStrategy.GuardWithIf
                )
            else:
                order = [
                    l[0] if l[0] != 'm' else l for l in S.order
                    if l[-1] == 'i' or l[0] == 'm']
                assert(S.order[4][0] == 'm')
                f.update().split(
                    self.m, self.m_o, self.m_i, S.T.m,
                    hl.TailStrategy.GuardWithIf
                )
            self.i = self.r.z

            # Apply Loop Reordering
            self.f.update().reorder(*map(lambda x: getattr(self, x), order))

            # store local buffer on outer tiling loop
            self.ldata.store_at(self.f, getattr(self, S.order[4]))
            # compute on inner to fold though
            self.ldata.compute_at(self.f, getattr(self, S.order[3][0]))
        else:
            # loop reorder conv function
            self.f.update().reorder(*map(lambda x: getattr(self, x), uorder))

            # keep local data for tile of output
            self.ldata.store_at(last.f, getattr(last, S.order[4]))
            # compute one lower to fold storage/compute
            # self.ldata.compute_at(last.f, getattr(last, S.order[3][0]))
            self.ldata.compute_at(self.f, getattr(self, S.order[2][0]))

            # compute convolution at same level as local buffer
            self.f.compute_at(last.f, getattr(last, S.order[3][0]))


class Conv2D_recomp(HalideFunc):
    """
    2D convolution function
    """

    def __init__(self, layer, input):

        # init halide func
        super().__init__(layer)

        # Load variables into shorthands
        b = self.b
        i = self.i
        o = self.o
        m = self.m
        n = self.n
        D = self.D
        S = self.S
        name = self.name

        # Load variables into shorthands
        use_bias = self.use_bias

        # get input and output tensors
        inputs = listify(layer.input)
        assert (
            len(inputs) == 1 and "Only support Conv2D with single input"
        )

        outputs = listify(layer.output)
        assert (
            len(outputs) == 1 and "Only support Conv2D with single output"
        )

        assert (
            layer.data_format in ['channels_last', 'NHWC'] and
            "Only NHWC layout supported"
        )

        # Get weights and biases from layer
        if use_bias:
            self.weights, self.bias = self.get_weights()
        else:
            self.weights = self.get_weights()[0]

        # Weights buffer
        self.weights_host = hl.Buffer(
            np.reshape(self.weights, (D.l, D.k, D.i, D.o)),
            name + '_weights_host'
        )
        self.weights_device = hl.Func(name + '_weights_device')
        self.weights_device[n, m, i, o] = self.weights_host[n, m, i, o]

        # Bias buffer
        self.bias_device = None
        if use_bias:
            self.bias_host = hl.Buffer(
                np.reshape(self.bias, (D.o)), name + '_bias_host'
            )
            self.bias_device = hl.Func(name + '_bias_device')
            self.bias_device[o] = self.bias_host[o]

        # Input buffer
        if isinstance(input, HalideFunc):
            self.input_unpadded = input.f
        elif isinstance(input, np.ndarray):
            self.input_unpadded = hl.Buffer(input, name + '_input_host')
        else:
            print(
                "Error: unknown input type %s passed to conv2d" %
                (type(input))
            )
            exit(-1)

        # Add padding to input
        if self.padding_type == 'same':
            # N.B.: buffer passed to condition can NOT be the same as the variable
            # that's written to (i.e. b_input != b_input_unpadded)
            self.input_data = hl.BoundaryConditions.constant_exterior(
                self.input_unpadded,
                0.,
                bounds=[(0, D.b), (0, self.in_height),
                        (0, self.in_width), (0, D.i)]
            )
        else:
            # valid padding
            self.input_data = self.input_unpadded

        # Main Function
        conv = hl.Func(name)

        # Reduction Domain
        r = hl.RDom(
            [
                (0, D.k),  # x
                (0, D.l),  # y
                (0, D.i),  # z
            ],
            name + "_kernel"
        )
        self.r = r

        # Load bias, or init with zeroes
        conv[b, n, m, o] = self.bias_device[o] if self.use_bias else 0.

        # Compute convolution
        conv[b, n, m, o] += \
            self.weights_device[r.y, r.x, r.z, o] * \
            self.input_data[b, n * S.n + r.y - self.pad_n,
                            m * S.m + r.x - self.pad_m, r.z]

        # Apply activation function if required
        if self.act.type == 'relu':
            conv[b, n, m, o] = hl.min(
                self.act.max_value, hl.max(0., conv[b, n, m, o])
            )

        # Set main function
        self.f = conv

        # Set output shape
        self.shape = [D.b, D.n, D.m, D.o]

    def schedule(
        self, S, last=None, fuse_tgt=None, fuse_sched=None, verbose=False
    ):
        # shorthands
        f = self.f
        weights_device = self.weights_device

        # Apply Tiling
        # f.update().split(self.n, self.n_o, self.n_i, S.T.n, hl.TailStrategy.RoundUp)
        # f.update().split(self.m, self.m_o, self.m_i, S.T.m, hl.TailStrategy.RoundUp)
        # f.update().split(self.o, self.o_o, self.o_i, S.T.o, hl.TailStrategy.RoundUp)
        f.update().split(
            self.n, self.n_o, self.n_i, S.T.n,
            hl.TailStrategy.GuardWithIf
        )
        f.update().split(
            self.m, self.m_o, self.m_i, S.T.m,
            hl.TailStrategy.GuardWithIf
        )
        f.update().split(
            self.o, self.o_o, self.o_i, S.T.o,
            hl.TailStrategy.GuardWithIf
        )

        if hasattr(S.T, 'i'):
            f.update().split(
                self.r.z, self.i_o, self.i_i, S.T.i,
                hl.TailStrategy.GuardWithIf
            )

        # Apply Loop Reordering
        f.update().reorder(*map(lambda x: getattr(self, x), S.order))

        # Schedule production of weights always into self
        if ((S.fold_dims(data=False) in [['i', 'o'], ['o', 'i']]) or
                (S.sl.w[0] in 'oi' and S.cl.w[0] in 'oi')):
            # Halide misses folds in these cases, move the sl to the cl
            # down towards compute level
            weights_device.store_at(f, getattr(self, S.cl.w))
            weights_device.compute_at(f, getattr(self, S.cl.w))
        else:
            weights_device.store_at(f, getattr(self, S.sl.w))
            weights_device.compute_at(f, getattr(self, S.cl.w))

        # Provide some hints to halide on folding
        if 'm' in S.fold_dims(data=False):
            weights_device.fold_storage(self.m, 1, fold_forward=True)
        if 'n' in S.fold_dims(data=False):
            weights_device.fold_storage(self.n, 1, fold_forward=True)

        # Get fuse target
        if fuse_tgt in [None, self]:
            return

        # get data store and compute levels
        comp_lvl = fuse_sched.cl.d
        store_lvl = fuse_sched.sl.d

        if verbose:
            print(
                'Fusing production of %s' % (self.name),
                'into',
                fuse_tgt.name,
                'compute at',
                comp_lvl,
                'store at',
                store_lvl,
            )

        # Fuse and schedule production of main function
        f.store_at(fuse_tgt.f, getattr(self, store_lvl))
        f.compute_at(fuse_tgt.f, getattr(self, comp_lvl))

        # Provide some hints to halide on folding
        if 'm' in fuse_sched.fold_dims():
            f.fold_storage(self.m, fuse_tgt.Km, fold_forward=True)
        if 'n' in fuse_sched.fold_dims():
            f.fold_storage(self.n, fuse_tgt.Kn, fold_forward=True)
        if 'o' in fuse_sched.fold_dims():
            f.fold_storage(self.o, 1, fold_forward=True)
