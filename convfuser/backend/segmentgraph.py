import types
import networkx as nx


class Segment(object):
    def __init__(
        self,
        G,
        nodes,
        schedule=None,
        buffered_input=None,
        recompute=True
    ):
        # set schedule
        self.schedule = schedule

        # set nodes
        self._nodes = nodes

        # Set subgraph
        self.G = G.subgraph(self._nodes)

        # Should input be completely buffered?
        self.buffered_input = buffered_input

        self.recompute = recompute

    @property
    def halide(self):
        # Halide node if a schedule is passed
        return self.schedule is not None

    @property
    def first(self):
        """Return first node"""
        # N.B., should only be 1
        for n in self.G:
            if self.G.in_degree(n) == 0:
                return n
        print(
            "Error: did not find entry point to Segment %s" % str(self)
        )
        exit(-1)

    @property
    def nodes(self):
        return [n for n in nx.dfs_preorder_nodes(self.G, self.first)]

    @property
    def input(self):
        """ Return inputs to this segment, i.e., to the first node"""
        return self.first.input

    @property
    def inputs(self):
        """ Return inputs to this segment based on cfg(config) of first node"""
        true_first = self.first
        while true_first.merged_in is not None:
            true_first = true_first.merged_in
        return true_first.cfg.inputs

    @property
    def name(self):
        return str([n.name for n in self.nodes])

    def __str__(self):
        return self.name

    def __repr__(self):
        return str(self)


def dot(self, fname):
    return nx.drawing.nx_pydot.write_dot(self, fname)


def SegmentGraph(G, segments):
    """Translate a graph into a Meta Graph based on segments"""

    # Create segment graph
    S = nx.DiGraph()

    # map from nodes to subgraph
    node2subgraph = {}

    # Create subgraphs for all nodes in a segment
    for seg in segments:
        sub = Segment(
            G, seg.nodes, seg.schedule, seg.buffered_input,
            seg.recompute
        )
        S.add_node(sub)
        for n in seg.nodes:
            node2subgraph[n] = sub

    # Create single node subgraphs for all remaining nodes
    for n in G.nodes:
        if n not in node2subgraph:
            sub = Segment(G, [n])
            S.add_node(sub)
            node2subgraph[n] = sub

    # loop over original nodes and connect segmentgraph
    for src_node in G.nodes:
        src_sub = node2subgraph[src_node]
        for dst_node in G.successors(src_node):
            dst_sub = node2subgraph[dst_node]
            if src_sub != dst_sub:
                # extract name of tensor from edge, and copy to segment graph
                data_name = G.get_edge_data(src_node, dst_node)['name']
                S.add_edge(src_sub, dst_sub, name=data_name)

    # Set input subgraph
    S.first = node2subgraph[G.first]

    S.dot = types.MethodType(dot, S)
    return S
