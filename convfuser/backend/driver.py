from .halidepipeline import HalidePipeline
from ..utils import delistify, listify, rmse
import tensorflow as tf
# from keras import backend as K
# import networkx as nx
from .segmentgraph import SegmentGraph


def exec_node(n, res):

    # Recursively handle any merged input nodes
    if n.merged_in is not None:
        res = exec_node(n.merged_in, res)

    # Compute node itself
    res = n(res)

    # Recursively handle any merged input nodes
    if n.merged_out is not None:
        res = exec_node(n.merged_out, res)

    return res


def exec_layer(n, trace=False, verbose=False, silent=False):
    if not silent:
        print('computing %s' % (n.name))

    # First execute with Keras
    res = exec_node(n, delistify(n.computed_inputs))

    # If not a conv2d layer, return result
    if n.type not in ['conv2d']:
        return res, (0, 0, 0, 0, 0)

    ref = res
    if not silent:
        print("Performing convolution using halide")

    # construct pipeline
    pipeline = HalidePipeline(
        delistify(n.computed_inputs), verbose=verbose
    )
    pipeline.chain(n)

    # compute output of pipeline
    res, meas = pipeline(trace=trace)

    # convert to tensor format
    res = tf.convert_to_tensor(res)

    # check RMSE versus reference
    err = rmse(ref, res)
    if err > 0.001:
        print('RMSE error', err)
        # diff = np.square(ref - res)
        # import matplotlib.pyplot as plt
        # diff = np.moveaxis(diff, 3, 1)
        # print('diff shape', diff.shape, 'max err', np.max(diff))
        # for idx, im in enumerate(diff[0, :, :, :]):
        #    print('plotting heatmap', idx, im.shape, np.max(im))
        #    plt.imshow(im, cmap='hot', interpolation='nearest')
        #    plt.show()
        exit(-1)

    return ref, meas


def exec_segment(seg, trace=False, verbose=False, silent=False):
    if not silent:
        print('Computing segment %s' % (seg.name))

    # Compute with Keras
    res = delistify(seg.computed_inputs)
    for n in seg.nodes:
        res = exec_node(n, res)

    # If there is no halide schedule, it ends here
    if not seg.halide:
        return res, (0, 0, 0, 0, 0)
    ref = res

    # Halide execution
    if not silent:
        print("Halide execution")

    # Construct pipeline from inputs
    pipeline = HalidePipeline(
        delistify(seg.computed_inputs),
        buffered_input=seg.buffered_input,
        recompute=seg.recompute,
        verbose=verbose
    )

    # chain in all the nodes
    pipeline.chain(seg.nodes)

    # schedule the pipeline
    pipeline.schedule(seg.schedule)

    # Save pipeline to html for debug
    # pipeline.save_html("%s.html" % (seg.name))

    # compute output of pipeline
    res, meas = pipeline(trace=trace, verbose=verbose)

    # convert to tensor format
    res = tf.convert_to_tensor(res)

    # check RMSE versus reference
    err = rmse(ref, res)
    if err > 0.001:
        print('RMSE error', err)
        exit(-1)
    else:
        if not silent:
            print('Halide output correct')

    # since they are about equal, and we want to get rid of integration
    # errors, lets forward the reference
    return ref, meas


def reduce_meas(m1, m2):
    return (
        m1[0] + m2[0],  # accesses
        max(m1[1], m2[1]),  # buffer
        m1[2] + m2[2],  # macs
    )


def list_schedule(
    G, input_data, exec_func, trace=False, silent=False, verbose=False
):
    """A list scheduler for graph execution"""

    # Set computed_inputs attribute on all nodes
    for n in G.nodes:
        n.computed_inputs = {}

    # Populate input node with input data
    G.first.computed_inputs = [input_data]

    # Add to ready list
    ready_list = [G.first]

    # Keep track of overall accesses and memory usage
    stats = (0, 0, 0)

    # Execute nodes until there are none left
    while ready_list != []:

        # get next node for execution
        n = ready_list.pop()

        # Execute the node somehow
        res, meas = exec_func(n, trace, verbose, silent)

        # Accumulate accsess, max of memory usage
        if meas is not None:
            acc = meas[0] + meas[1]
            mem = meas[2] + meas[3]
            macs = meas[4]
            if verbose:
                print('Measurements of segment:', n.name)
                print('Accesses:', acc)
                print('Memory:', mem)
                print('Multiply Accumulates:', macs)
            stats = reduce_meas(stats, (acc, mem, macs))

        # Update the inputs of the successors
        for suc in G.successors(n):
            # Ignore any self loops (should not be there in the first place!)
            if suc == n:
                continue

            # Get name of computed tensor
            data_name = G.get_edge_data(n, suc)['name']

            # Add data to computed_inputs
            suc.computed_inputs[data_name] = res

            # Check if successor is ready for execution
            if len(suc.computed_inputs) == len(listify(suc.inputs)):
                # Resolve computed_tensor order
                suc.computed_inputs = [
                    suc.computed_inputs[inp.name]
                    for inp in listify(suc.inputs)
                ]

                # Add successor to ready list
                ready_list.insert(0, suc)

    # Return final result
    return res.numpy(), stats


def run(
    G,
    segments,
    input_data,
    trace=False,
    verbose=False,
    silent=False,
    seg_dot=None
):

    # transform layer graph into segment graph
    S = SegmentGraph(G, segments)

    # Print segment graph
    if seg_dot:
        S.dot(seg_dot)

    # Execute segment graph with list scheduler
    ret, stats = list_schedule(
        S,
        input_data,
        exec_segment,
        trace=trace,
        verbose=verbose,
        silent=silent
    )

    # Return result
    return ret, stats
    return ret, stats
