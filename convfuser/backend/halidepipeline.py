import halide as hl
import numpy as np
from os import environ
from .conv2d import Conv2D, Conv2D_recomp
from .input import Input, Input_recomp
from collections import defaultdict
from ..utils import max2consec


class OrderedSet(list):
    """Thin (low-performance) ordered set"""

    def add(self, e):
        if e not in self:
            self += [e]


class HalidePipeline(object):
    def __init__(
        self,
        input,
        buffered_input=False,
        recompute=True,
        verbose=False
    ):
        # run verbose
        self.verbose = verbose

        # By default not tracing any buffers
        self.trace_buffers = defaultdict(OrderedSet)

        # Get jit target
        self.jit_tgt = hl.get_jit_target_from_environment()

        # Set wether to buffer the input or not
        self.buffered_input = buffered_input

        # Set if recompute schedule, or full buffered
        self.recompute = recompute

        # Not yet scheduled
        self.scheduled = False

        # If the input is not buffered, actually execute and count it
        if not self.buffered_input:
            # Instantiate input layer
            f_input = Input_recomp(input) if self.recompute else Input(input)

            # Add tracing of relevant function
            self._trace_input(f_input)

            # Add input to list of layers in the pipeline
            self._layers = [f_input]
        else:
            # buffered, pass the input object
            if hasattr(input, 'numpy'):
                input = input.numpy()
            self._layers = [input]

    @property
    def last(self):
        return self._layers[-1]

    @property
    def first(self):
        return self._layers[0]

    @property
    def f(self):
        return self.last.f

    @property
    def shape(self):
        return self.last.shape

    def __str__(self):
        return '->'.join(map(str, self._layers))

    def _trace_input(self, input):
        self.trace_buffers['input_host'].add(input.input_host.name())
        self.trace_buffers['input_dev'].add(input.input_device.name())

    def _trace_conv2d(self, conv2d):
        self.trace_buffers['weights_host'].add(
            conv2d.weights_host.name()
        )
        self.trace_buffers['weights_dev'].add(
            conv2d.weights_device.name()
        )
        if hasattr(conv2d, 'bias_host'):
            self.trace_buffers['bias_host'].add(conv2d.bias_host.name())
        if hasattr(conv2d, 'ldata'):
            self.trace_buffers['data'].add(conv2d.ldata.name())
        else:
            self.trace_buffers['data'].add(conv2d.f.name())

    @property
    def trace_names(self):
        for k, st in self.trace_buffers.items():
            for n in st:
                yield n

    # Helper func to map getter func to trace buffer category
    def __get_trace(self, cat, getter, fred=sum):
        """Get single trace"""
        return fred(
            filter(
                lambda x: x >= 0, map(getter, self.trace_buffers[cat])
            )
        )

    def get_trace_results(self, verbose=False):
        # Shorthand for output func
        f = self.f

        if verbose:
            from pprint import pprint
            # Memory sizes
            mem = dict((b, f.get_mem_size(b) // 4)
                       for b in self.trace_names
                       if f.get_mem_size(b) != -1)
            print("Memory sizes:")
            f.print_mem_stats()
            # pprint(mem)

            # Memory Accesses
            acc = {}
            acc = dict((b, f.get_loads(b))
                       for b in self.trace_names
                       if f.get_loads(b) != -1)
            print("Accesses:")
            f.print_counters()
            # pprint(acc)

        # Get number of data accesses
        dacc = self.__get_trace('input_host', f.get_loads)

        # Get weight accesses
        wacc = self.__get_trace('weights_host', f.get_loads)

        # Get memory sizes, assuming all are alive simultaneously
        dbuf_in = [self.__get_trace('input_dev', f.get_mem_size)]
        dbuf_d = list(
            self.__get_trace('data', f.get_mem_size, fred=lambda x: x)
        )
        dbuf = max2consec(
            dbuf_in + dbuf_d
        ) // 4  # only two buffers alive at a time
        # N.B.: for this max2consex the ordered set is required!

        # Get weight memory size, also assuming all live!
        wbuf = self.__get_trace(
            'weights_dev', f.get_mem_size, fred=max
        ) // 4

        # Add storage and accesses of fully buffered layer if required
        if self.buffered_input:
            dacc += self.first.size  # load all input elements once
            dbuf += self.first.size  # store all input elements

        # get multiply accumulates
        macs = self.__get_trace('weights_dev', f.get_loads)

        # Group
        traced = (dacc, wacc, dbuf, wbuf, macs)

        # Debug print
        # for var in ['dacc', 'wacc', 'dbuf', 'wbuf', 'macs']:
        #    print(var + ':', locals()[var])

        return traced

    def save_html(self, fname):
        assert (self.f is not None)
        self.f.compile_to_lowered_stmt(fname, [])

    def chain(self, layers):
        """ Chain more layers """
        for lyr in layers:
            assert (
                lyr.type == 'conv2d'
                and "Only support fusion of conv2d layers for now"
            )
            # Chain current output function self.f as input to new function
            if self.recompute:
                conv2d = Conv2D_recomp(lyr, self.last)
            else:
                conv2d = Conv2D(lyr, self.last)
                # todo custom trace

            self._trace_conv2d(conv2d)
            self._layers += [conv2d]

    def schedule(self, schedules):
        # check no schedule has been applied yet
        assert ((not self.scheduled) and "can only schedule once")

        # Ensure there is a schedule for each conv2d layer
        assert (len(schedules) == len(self._layers) - 1)

        # Iterate over layers from last to input
        # Propagate fuse target and store level upwards
        fuse_tgt = None
        fuse_sched = None
        for S, l in zip(reversed(schedules), reversed(self._layers)):
            l.schedule(
                S,
                self.last,  # last layer in fused segment
                fuse_tgt=fuse_tgt,  # next layer
                fuse_sched=fuse_sched if self.recompute else None,
                verbose=self.verbose,
            )
            fuse_tgt = l
            fuse_sched = S

        # schedule the input layer if unbuffered
        if not self.buffered_input:
            self._layers[0].schedule(
                fuse_tgt=fuse_tgt,
                fuse_sched=fuse_sched,
            )

        if self.verbose:
            self.f.print_loop_nest()

        # indicate scheduling is done
        self.scheduled = True

    def __call__(self, trace=False, **kwargs):
        # Get halide function
        assert (self.f is not None)
        f = self.f

        # sanity check
        if not self.schedule:
            print(
                "Warning executing unscheduled pipeline.\n",
                "Default halide schedule will be used."
            )

        # Get jit context
        jit = self.jit_tgt

        # Possibly enable tracing
        if trace:

            # Init access counters
            f.count_accesses_unsafe()
            for b in self.trace_names:
                f.init_counter(b)

            # enable memory tracing
            f.trace_mem()
            # def p(s):
            #    print("PY:", s)
            # f.set_custom_print(p)

        if trace or (environ.get('HL_TRACE_FILE') is not None):
            # update jit target
            jit = jit.with_feature(
                hl.TargetFeature.TraceLoads
            ).with_feature(hl.TargetFeature.Profile)\
                .with_feature(
                hl.TargetFeature.TraceStores
            )

        # Realize
        output = f.realize(*self.shape, jit)
        res = np.asarray(output)

        # Tracing results
        traced = None if not trace else self.get_trace_results()

        return res, traced
