import networkx as nx
from .schedule import DesignSpace, Network
from .sequence import sequence_space
from ..utils import pareto
from functools import reduce
from itertools import product
import numpy as np


def sequences(G, allowed=['conv2d']):
    """ Iterator over all sequences of allowed layers in a network O(n) """
    seq = []

    # Traverse nodes in DFS order
    for n in nx.traversal.depth_first_search.dfs_preorder_nodes(G, G.first):
        # if not valid node for merging
        if not ((n.type in allowed) and G.in_degree(n) <= 1 and G.out_degree(n) <= 1):
            # emit current sequence if any
            if seq != []:
                yield seq
                seq = []
            continue

        # valid node
        if seq != []:
            # continuation of sequence
            if G.has_edge(seq[-1], n):
                seq += [n]
                continue
            # start of new sequence, emit old
            yield seq

        # new start
        seq = [n]

    # Finalize outstanding sequence
    if seq != []:
        yield seq


def add_points(a, b):
    """ Helper function that merges two points"""
    return tuple((
        a[0] + b[0],  # add accumulation
        max(a[1], b[1]),  # max mem
        a[2] + b[2],  # add macs
        a[3] + b[3],  # concat segment schedules
    ))


def network_space(G, args=None):
    """Get pareto schedules for graph"""

    # Get pareto schedules of each sequence
    seq_costs = [sequence_space(sequence, args=args) for sequence in sequences(G)]

    # Try all combinations of schedules between sequences
    print('Combining segments for entire network')

    def reduce_seq(a, b):
        cost = set()
        for pa, pb in product(a, b):
            p = add_points(pa, pb)
            cost.add(p)
        if args.pareto:
            return pareto(np.array(list(cost)))
        return np.array(list(cost))
    points = reduce(reduce_seq, seq_costs)

    # Wrap points in some pretty classes
    return DesignSpace([Network(pt) for pt in points])


