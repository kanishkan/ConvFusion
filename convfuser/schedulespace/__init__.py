import numpy as np
from .network import network_space as scheduling_space
from .schedule import DesignSpace
