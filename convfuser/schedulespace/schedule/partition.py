from ...utils import InvisibleList
from .segment import Segment


class Partition(InvisibleList):
    """ A list of segment schedules"""

    def __repr__(self):
        return 'Part[%s]' % (','.join(map(str, self)))

    def __str__(self):
        return self.__repr__()

    def __add__(self, other):
        if not isinstance(other, self.__class__):
            return self.__class__(super().__add__([other]))
        return self.__class__(super().__add__(other))

    @property
    def json(self):
        return [seg.json for seg in self]

    @classmethod
    def from_json(cls, obj, name2node):
        return cls(
            [Segment.from_json(segjson, name2node) for segjson in obj]
        )

