from .designspace import DS as DesignSpace
from .layer import Layer
from .segment import Segment
from .partition import Partition
from .network import Network
