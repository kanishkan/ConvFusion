from .network import Network
import matplotlib.pyplot as plt
# from matplotlib.ticker import ScalarFormatter
import numpy as np
import json
from ...utils import tex_escape
# from math import log


class DS(list):
    """Design Space: List of NetworkPoints"""
    @property
    def json(self):
        return [networkpoint.json for networkpoint in self]

    @classmethod
    def from_json(cls, nw, obj):
        name2node = dict((n.name, n) for n in nw.nodes)
        return cls([
            Network.from_json(networkpoint_json, name2node)
            for networkpoint_json in obj
        ])

    def save(self, fname):
        with open(fname, 'wt') as f:
            f.write(json.dumps(self.json))

    @classmethod
    def load(cls, nw, fname):
        with open(fname, 'rt') as f:
            ds = cls.from_json(nw, json.loads(f.read()))
        return ds

    def __plot(self, fig_ax=None, tex_compat=False):
        # Cast to numpy array for convenient accesses
        points = np.array(self)

        # separate schedules
        accesses = points[:, 0]
        mem_size = points[:, 1]
        macs = points[:, 2]
        schedules = points[:, -1]

        # Create figure
        if fig_ax is not None:
            fig, ax = fig_ax
        else:
            plt.close()
            fig, ax = plt.subplots()

        # solarized colors
        sbase03 = '#002B36'
        sbase02 = '#073642'
        sbase01 = '#586E75'
        sbase00 = '#657B83'
        sbase0 = '#839496'
        sbase1 = '#93A1A1'
        sbase2 = '#EEE8D5'
        sbase3 = '#FDF6E3'
        syellow = '#B58900'
        sorange = '#CB4B16'
        sred = '#DC322F'
        smagenta = '#D33682'
        sviolet = '#6C71C4'
        sblue = '#268BD2'
        scyan = '#2AA198'
        sgreen = '#859900'

        # Set plot background color
        ax.set_facecolor(sbase3)

        # scale memory to 32b values (4 bytes)
        # mem_size *= 4

        # select order of magnitude to use in plot
        # ord_char = [''] + list('kMGT')
        # order_mag = int(max(0, min(len(ord_char) - 1, log(max(mem_size), 10) // 3) - 1))
        # mem_size /= pow(10, order_mag * 3)
        # mem_unit = ord_char[order_mag] + 'Features'
        mem_size /= 1000000.
        mem_unit = 'Mega Features'

        # compensate for add layer in df paper
        # print(accesses)
        # accesses += 2162 * 3182 * 3
        # print(accesses)

        # Plot space with ticker
        cmap_options = ['plasma', 'hot', 'afmhot']
        cmap_idx = -1 if not tex_compat else -2
        cmap = cmap_options[cmap_idx]
        cax = ax.scatter(
            mem_size, accesses, c=macs, cmap=cmap, picker=True
        )

        # Set text filter function
        txt_filt = (lambda s: s) if not tex_compat else tex_escape

        # Set axis titles
        plt.xlabel(txt_filt("Internal Memory Size [%s]" % (mem_unit)))
        plt.ylabel(txt_filt("External Accesses [#]"))
        plt.yscale('log', base=10)
        plt.xscale('log', base=10)

        def fmt(val, idx):
            return int(val) if float(val) >= 1.0 else float(val)

        ax.xaxis.set_major_formatter(fmt)
        ax.yaxis.set_minor_formatter(lambda val, idx: '')
        ax.set_axisbelow(True)
        gridc = 'gray'
        plt.grid(color=gridc, linewidth=1, which='major', linestyle='-')
        plt.grid(
            color=gridc,
            linewidth=1,
            which='minor',
            linestyle='dashed',
            axis='y'
        )
        cbar = fig.colorbar(cax)
        cbar.set_label(
            txt_filt('Multiply-Accumulates')
        )  # , rotation=-90)

        return fig, ax

    def select_point(self):
        # Create plot
        fig, ax = self.__plot()

        # Set title
        plt.title("Select point to run")

        # Callack on pick
        idx = None

        def onpick(event):
            nonlocal idx
            idx = event.ind[0]
            plt.close()

        # Attach callback and wait for user to select
        fig.canvas.mpl_connect('pick_event', onpick)
        plt.show()

        return idx

    def plot(self, fname=None, fig_ax=None, show=True):
        # True if output is tex
        latex = fname.endswith('.tex') or fname.endswith('.tikz')

        # Plot figure
        fig, ax = self.__plot(fig_ax, tex_compat=latex)

        # Set title
        if not latex:
            plt.title("Scheduling Space")

        # Just show if there is no filename
        if show is True:
            plt.show()

        # Save as figure
        if fname is not None:
            # Try to import tikzplot library
            if latex:
                try:
                    import tikzplotlib
                except ImportError:
                    print(
                        "Please install tikzplotlib package to plot tex figures"
                    )
                    exit(-1)

                # Clean figure
                tikzplotlib.clean_figure(fig=fig)

            # tex extra parameters
            extra_axis_parameters = [
                'axis line style = {thick}',
                'colorbar style = {thick}',
                'tick style = {thick}',
            ]
            extra_tikzpicture_parameters = ['scale=0.88']

            # Save figure
            if fname.endswith('.tikz'):
                tikzplotlib.save(
                    fname,
                    figure=fig,
                    extra_axis_parameters=extra_axis_parameters,
                    extra_tikzpicture_parameters=extra_tikzpicture_parameters
                )
            elif fname.endswith('.tex'):
                tikzplotlib.save(
                    fname,
                    figure=fig,
                    standalone=True,
                    extra_axis_parameters=extra_axis_parameters,
                    extra_tikzpicture_parameters=extra_tikzpicture_parameters
                )
            else:
                fig.savefig(fname)

            # Inform user
            print("Saved schedule space to '%s'" % (fname))

        return (fig, ax)
