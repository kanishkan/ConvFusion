from .schedule import Layer
import numpy as np
from itertools import permutations, product
from ..utils import iceil, log


def pow2tiles(d):
    """ Generate valid power of 2 tile sizes for dimension d """
    return map(lambda p: 2**p, range(iceil(log(d, 2)) + 1))


def wholetiles(d):
    """ Generate tiles which are exact divisors of the dimension"""
    # for div in reversed(range(1, d + 1)):
    for div in range(1, d + 1):
        if (d % div) == 0:
            yield d // div


def extremestiles(d):
    """ Generate tiles of 1 and full dimension"""
    yield d
    yield 1


def notiling(d):
    """ Generate tiles fit the entire dimension (effectively no tiling)"""
    yield d


def layer_space(
    l,  # output layer
    tiling_func=wholetiles
):
    vol = l.cfg.vol

    # no recompute, store all root
    sl = (4, 4)

    # tiling in m
    order = [3, 2, 0, 1, 4, 5, 7, 6]
    for Tm in tiling_func(vol[0]):
        T = (Tm, vol[1], vol[2], vol[3])
        yield Layer(sl, order, T)

    # tiling in n
    order = [3, 2, 1, 0, 5, 4, 7, 6]
    for Tn in tiling_func(vol[1]):
        T = (vol[0], Tn, vol[2], vol[3])
        yield Layer(sl, order, T)


def layer_space_recompute(
    l,  # input layers
    prod_vol=None,
    tiling_func=wholetiles,
    loopreorder=True,
):
    """ Scheduling space of a single layer """

    l = l.cfg

    # Production volume equals layer output if not given
    if prod_vol is None:
        prod_vol = l.vol

    # number of pure dimensions (not tiled)
    vol = prod_vol
    dims = len(vol)

    # loop order gen
    def order_gen(weight_sl):
        outer_lvls = list(range(dims, 2 * dims))

        if not loopreorder:
            # fix loop order to m_i,n_i,i_i,o_i,m_o,n_o,i_o,o_o
            if dims == 4:
                yield [0, 1, 3, 2, 4, 5, 7, 6]
            else:
                yield [0, 1, 2, 4, 5, 6]
            return

        # Inner loop
        for under_sl in permutations(range(dims), min(dims, weight_sl + 1)):
            above_sl = filter(lambda i: i not in under_sl, range(dims))
            inner = list(under_sl) + list(above_sl)

            # Outer loop lvl
            if weight_sl < dims:
                # outer loop order doesn't matter
                yield inner + outer_lvls
            else:
                for sel in range(dims):
                    rem = [outer_lvls[idx] for idx in range(dims) if idx != sel]
                    yield inner + [outer_lvls[sel]] + rem

    # Space of tiling
    def tiling_gen(sl, order):
        def tile_gen(lvl):
            lvl_pos = order[lvl]
            if lvl_pos <= sl:
                return tiling_func(vol[lvl])
            return [vol[lvl]]  # no need to tile, just make it full size inner

        return product(*map(tile_gen, range(dims)))

    # space of store levels
    min_sl = 0
    max_sl = dims + 1
    for weight in reversed(range(min_sl, max_sl)):
        for buf in reversed(range(min_sl, weight + 1)):
            sl = (weight, buf)

            # add loop order
            for order in order_gen(weight):

                # add tiling
                for T in tiling_gen(weight, order):

                    # Finally yield schedule
                    yield Layer(sl, order, T)
