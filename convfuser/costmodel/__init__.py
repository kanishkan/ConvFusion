# Expose functions directly by their name, rather than through the module
from .network import network
from .layer import layer
from .segment import segment
