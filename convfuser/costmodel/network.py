from .segment import segment as segment_cost
from functools import reduce


def network(sched, ignore_output=False, verbose=False, silent=True):

    if not silent:
        verbose = True

    # Wrapper for verbose argument
    def cost_seg(seg):
        return segment_cost(seg, ignore_output, verbose)

    # reduction function of two segments
    def merge(a, b):
        acc1, mem1, macs1 = a
        acc2, mem2, macs2 = b
        return ((acc1 + acc2), max(mem1, mem2), macs1 + macs2)

    # reduce cost over all segments
    # return accesses, memory size
    return reduce(merge, map(cost_seg, sched))
