from .layer import layer as layer_cost
from ..utils import max2consec
from functools import reduce
import operator as op


def segment(seg, ignore_output=False, verbose=False):
    """ Cost model of a segment """
    if seg.recompute:
        return segment_recompute(
            seg, ignore_output=ignore_output, verbose=verbose
        )

    # Iterate over nodes from  producer to consumer
    _dacc = 0
    _wacc = 0
    _oacc = 0
    _dbuf = 0
    _wbuf = 0
    _obuf = 0
    _macs = 0

    # iterate front to back
    for idx, (node, sched) in enumerate(list(seg.node_sched())):
        cons_vol, cons_vol_folded, times, times_folded, wacc, oacc, \
            dbuf, wbuf, obuf, macs = \
            layer_cost(node, sched, prod_vol=None)
        input_vol = node.cfg.pad_cons_vol.size

        # if replicate_depth_first_paper:
        load_store_factor = 2
        if False:
            #print('Warning: imitating depth first paper which ignores several factors!')
            # forget about weights
            wbuf = 0
            wacc = 0
            # forget you need to both store and load overlap of tiles
            load_store_factor = 1

        # first layer needs to transfer complete input, other only the penalty
        if idx == 0:
            _dacc += (cons_vol.size * times)
        else:
            _dacc += (((cons_vol.size * times) - input_vol) * load_store_factor)
        _wacc += wbuf  # for no recompute we only load the weights once!
        _dbuf += dbuf
        _wbuf += wbuf  # TODO: update paper formula accordingly, weights are stored _root_
        # TODO also for macs update paper
        _macs += node.cfg.prod_vol.size * node.cfg.Di * node.cfg.Km * node.cfg.Kn
        # only last layer's output accesses/buffer count
        _oacc = oacc
        _obuf = obuf

    # Compute totals
    _oacc = (0 if ignore_output else _oacc)
    _obuf = (0 if ignore_output else _obuf)
    accesses = _wacc + _dacc + _oacc
    memory = _wbuf + _dbuf + _obuf
    macs = _macs

    if verbose:
        print('\n'.join([
            'Recompute: False',
            '',
            'total data accesses: %d' % _dacc,
            'total weight accesses: %d' % _wacc,
            'total output accesses: %d' % _oacc,
            'total accesses: %d' % accesses,
            '',
            'total data buffer: %d' % _dbuf,
            'total weight buffer: %d' % _wbuf,
            'total output buffer: %d' % _obuf,
            'Total buffer size: %d' % memory,
            '',
            'total multiply accumulates: %d' % macs,
            '*' * 30
        ]))

    return (accesses, memory, macs)


def segment_recompute(seg, ignore_output=False, verbose=False):
    """ Cost model of a segment with recomputation"""

    # Print name of segment
    if verbose:
        print('*' * 30)
        print('* Segment: [%s]' % ','.join(seg.names))

    _prod_vol = [(None, None)]
    _times = []
    _wacc = []
    _oacc = 0
    _dbuf = []
    _wbuf = []
    _obuf = 0
    _macs = []

    # Iterate over nodes from consumer to producer
    for node, sched in reversed(list(seg.node_sched())):

        # compute cost for layer
        cons_vol, cons_vol_folded, times, times_folded, wacc, oacc, \
            dbuf, wbuf, obuf, macs = layer_cost(node, sched, prod_vol=_prod_vol[-1][-1])  # select folded prod vol

        # ignore output contributions if requested
        if ignore_output:
            oacc = 0
            obuf = 0

        # add to segment variables
        _times += [(times, times_folded)]
        _wacc += [wacc]
        _oacc += oacc
        _dbuf += [dbuf]
        _wbuf += [wbuf]
        _obuf = max(_obuf, obuf)  # TODO: shouldn't this just be the last layer in segment??
        _macs += [macs]
        _prod_vol += [(cons_vol, cons_vol_folded)]

        # s+=['cons_vol_folded:', cons_vol_folded]
        # s+=['times_folded', times_folded]
        if not verbose:
            continue

        # Print cost of node
        print('\n'.join([
            '-' * 30,
            'Node: %s' % (node.name),
            ','.join([
                'Data Accesses: %d' % (cons_vol.size * times),
                'volume %d' % cons_vol.size,
                'times %d' % times
            ]),
            'Weight accesses: %d' % wacc,
            '(Partial) output accesses: %d' % oacc,
            'Input data memory: %d' % dbuf,
            'Weight memory: %d' % wbuf,
            'Output data memory: %d' % obuf,
            'Muliply accumulates: %d' % macs,
            '-' * 30,
        ]))

    # select folded times everywhere, except last one
    _times = [t[-1] for t in _times[:-1]] + [_times[-1][0]]

    # select non-folded production volume for final transfer calculations
    prod_vol = _prod_vol[-1][0]

    # Accesses
    scan_times = reduce(lambda l, e: l + [l[-1] * e], [[1]] + _times)
    wacc = sum(map(op.mul, scan_times, _wacc))

    # MACS
    macs = sum(map(op.mul, scan_times, _macs))

    # Buffer sizes
    wbuf = max(_wbuf)

    # Correct when input is completely buffered
    if seg.buffered_input:
        cvol = seg.nodes[0].cfg.cons_vol.size
        dacc = cvol
        dbuf = max2consec(_dbuf[:-1]) + cvol
    else:
        dacc = reduce(op.mul, _times) * reduce(op.mul, prod_vol)
        dbuf = max2consec(_dbuf)

    # for clarity, jsut copy private to regular names
    oacc = _oacc
    obuf = _obuf

    # Compute totals
    accesses = wacc + dacc + oacc
    memory = wbuf + dbuf + obuf

    if verbose:
        print('\n'.join([
            'Recompute: True',
            'Input: %sbuffered' % ('' if seg.buffered_input else 'un'),
            '',
            'total data accesses: %d' % dacc,
            'total weight accesses: %d' % wacc,
            'total output accesses: %d' % oacc,
            'total accesses: %d' % accesses,
            '',
            'total data buffer: %d' % dbuf,
            'total weight buffer: %d' % wbuf,
            'total output buffer: %d' % obuf,
            'Total buffer size: %d' % memory,
            '',
            'total multiply accumulates: %d' % macs,
            '*' * 30
        ]))

    return (accesses, memory, macs)
