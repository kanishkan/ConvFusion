import operator as op
from functools import reduce
import numpy as np
from ..utils import Namespace, ifloor, listify


class Volume(list):
    @property
    def dim_names(self):
        return 'mno'

    @property
    def D(self):
        return NameSpace(**dict(zip(self.dim_names, self)))

    @property
    def size(self):
        return reduce(op.mul, self)


class LayerCfgBase(object):
    def __init__(self, layer):
        self.inputs = listify(layer.input)
        self.outputs = listify(layer.output)


class LayerCfgConv2D(LayerCfgBase):
    """Class to represent layer configuration in terminology of paper"""

    def __init__(self, layer):

        super().__init__(layer)

        # get layer type from keras class name
        self.type = layer.__class__.__name__

        # Assert layer type is correct
        assert (self.type == 'Conv2D' and "Only Conv2D for now")

        # get input and output tensors
        assert (
            len(self.inputs) == 1
            and "Only support Conv2D with single input"
        )

        assert (
            len(self.outputs) == 1
            and "Only support Conv2D with single output"
        )

        # Sanity checks
        assert (
            layer.dilation_rate == (1, 1)
            and "Only dilation_rate of (1, 1) supported"
        )
        assert (layer.groups == 1 and "Only support groups=1 for now")

        # Get activation function (if any)
        self.act = Namespace(
            type=layer.activation.__name__.lower()
            if layer.activation else None,
            max_value=np.inf,
            # neg_slope=0.,
            # threshold=0.,
        )
        assert (
            self.act.type in [None, 'linear', 'relu']
            and "Only layers with linear or relu activation supported"
        )

        # set name of this layer
        self.name = layer.name

        # Get dimensions and define shorthands
        self.in_batchsize, self.in_height, self.in_width, self.Di = \
            self.inputs[0].shape
        self.Dl, self.Dk = layer.kernel_size
        self.out_batchsize, self.Dn, self.Dm, self.Do = self.outputs[
            0].shape

        assert (
            self.in_batchsize == self.out_batchsize
            and "Input batchsize does not match output batchsize"
        )
        self.in_batchsize = self.in_batchsize if self.in_batchsize else 1
        self.Db = self.in_batchsize

        # copy some more properties
        self.use_bias = layer.use_bias
        self.padding_type = layer.padding.lower()

        # compute default padding
        if self.padding_type == 'same':
            self.pad_n = ifloor(float(self.Dl) / 2.)
            self.pad_m = ifloor(float(self.Dk) / 2.)
        else:
            self.pad_n = 0
            self.pad_m = 0

        # Make dimensions directly accesible
        self.D = Namespace(
            b=self.Db,
            o=self.Do,
            i=self.Di,
            m=self.Dm,
            n=self.Dn,
            k=self.Dk,
            l=self.Dl
        )
        self._D = (self.Dm, self.Dn, self.Do, self.Di)

        # index kernel sizes also by mn
        self.Km = self.Dk
        self.Kn = self.Dl
        self.K = Namespace(
            m=self.Km,
            n=self.Kn,
        )
        self._K = (self.Km, self.Kn)

        # strides
        self.Sn, self.Sm = layer.strides
        self.S = Namespace(
            m=self.Sm,
            n=self.Sn,
        )
        self._S = (self.Sm, self.Sn)

        # Used to override weights when merging batchnorm
        self.get_weights = layer.get_weights

        # some more sanity checks
        assert (
            self.Do == layer.filters
            and "output shape does not match filter count"
        )

    @property
    def prod_vol(self):
        """ Get production volume of layer (Dm,Dn,Do)"""
        return Volume([self.Dm, self.Dn, self.Do])

    @property
    def vol(self):
        """ Get total volume of layer (Dm,Dn,Do,Di)"""
        return self.prod_vol + [self.Di]

    @property
    def cons_vol(self):
        """ Consumption volume (Dm,Dn,Di) """
        return Volume([self.in_width, self.in_height, self.Di])

    @property
    def pad_cons_vol(self):
        """ Consumption volume (Dm,Dn,Di) """
        return Volume([
            self.in_width + 2 * self.pad_m,
            self.in_height + 2 * self.pad_n, self.Di
        ])

    @property
    def padding(self):
        return self.pad_n, self.pad_m

    @padding.setter
    def padding(self, padding):
        # Update padding through padding layer attribute
        if isinstance(padding, type(((0, 0), (0, 0)))):
            ((self.pad_n, _), (self.pad_m, _)) = padding
        elif isinstance(padding, type((0, 0))):
            self.pad_n, self.pad_m = padding
        elif isinstance(padding, int):
            self.pad_m = self.pad_n = padding
        else:
            print("ERROR: unknown padding format:", padding)
            exit(-1)

    def __str__(self):
        s = ["Layer config (%s)" % (self.name)]
        for d in 'oimnkl':
            s += ["  D%c: %d" % (d, getattr(self.D, d))]
        for d in 'nm':
            s += ["  S%c: %d" % (d, getattr(self.S, d))]
        s += ["  Padding: %s" % (self.padding_type)]
        s += ["  Bias: %s" % (self.use_bias)]
        return "\n".join(s)
